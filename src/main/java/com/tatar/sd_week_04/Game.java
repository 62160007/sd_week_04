/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_04;

import java.util.*;

/**
 *
 * @author Dell 62160007 Thanawat Thongtitcharoen Sec 2
 */
public class Game {

    Scanner kb = new Scanner(System.in);
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int counter;
    int row, col;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void run() {
        this.showWelcome();
        counter = 0;
        do {
            this.showTable();
            this.showTurn();
            this.input();
            if (!table.checkWin()) {
                break;
            }
            table.switchPlayer();
        } while (counter <= 8);
        this.showResult(table.checkWin());
        this.table.setStatWinLose(counter);
        this.showPlayerScore();
        continueGame();
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();

    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                counter++;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }

    public void showResult(boolean checkwin) {
        this.showTable();
        if (checkwin) {
            System.out.println("No player Win..." + "\nBye bye ....");
        } else {
            System.out.println("Player " + table.getCurrentPlayer().getName()
                    + " Win..." + "\nBye bye ....");
        }
    }

    public void showPlayerScore() {
        System.out.println("Player X and Player O Score board!");
        System.out.println("Player X win :" + playerX.getWin());
        System.out.println("         lose:" + playerX.getLose());
        System.out.println("         Draw:" + playerX.getDraw());
        System.out.println("***********************************");
        System.out.println("Player O win :" + playerO.getWin());
        System.out.println("         lose:" + playerO.getLose());
        System.out.println("         Draw:" + playerO.getDraw());
    }

    public void reMat() {
        this.table.reTable();
        counter = 0;
    }

    public void continueGame() {
        System.out.println("You want to play again Y/N (Yes/No): ");
        char ans = kb.next().charAt(0);
        if (ans == 'Y' || ans == 'y') {
            this.reMat();
            this.run();
        }
    }
}
