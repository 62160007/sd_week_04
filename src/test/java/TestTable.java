/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.tatar.sd_week_04.Player;
import com.tatar.sd_week_04.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Dell
 */
public class TestTable {
    
    public TestTable() {
    }

    /**
     *
     * @throws Exception
     */
    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    /*
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }*/

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test 
    public void testRow1ByX(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('X', table.getCurrentPlayer().getName());
    }
    
    public void testRow2ByX(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('X', table.getCurrentPlayer().getName());
    }
    
    public void testRow3ByX(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('X', table.getCurrentPlayer().getName());
    }
    
    public void testCol1ByX(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('X', table.getCurrentPlayer().getName());
    }
    
    public void testCol2ByX(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('X', table.getCurrentPlayer().getName());
    }
    
    public void testCol3ByX(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('X', table.getCurrentPlayer().getName());
    }
    
    public void testX1ByX(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('X', table.getCurrentPlayer().getName());
    }
    
    public void testX2ByX(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('X', table.getCurrentPlayer().getName());
    }
    
    public void testRow1ByO(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('O', table.getCurrentPlayer().getName());
    }
    
    public void testRow2ByO(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('O', table.getCurrentPlayer().getName());
    }
    
    public void testRow3ByO(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('O', table.getCurrentPlayer().getName());
    }
    
    public void testCol1ByO(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('O', table.getCurrentPlayer().getName());
    }
    
    public void testCol2ByO(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('O', table.getCurrentPlayer().getName());
    }
    
    public void testCol3ByO(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('O', table.getCurrentPlayer().getName());
    }
    
    public void testX1ByO(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('O', table.getCurrentPlayer().getName());
    }
    
    public void testX2ByO(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(false, table.checkWin());
        assertEquals('O', table.getCurrentPlayer().getName());
    }
    
    public void testNobing1ByX(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.checkWin());
    }
    
    public void testNobing2ByX(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 0);
        table.setRowCol(1, 2);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.checkWin());
    }
    
    public void testNobing1ByO(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.checkWin());
    }
    
    public void testNobing2ByO(){
        Player x =new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 1);
        table.setRowCol(1, 0);
        table.setRowCol(1, 2);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.checkWin());
    }
}
